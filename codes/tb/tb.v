module tb();
	reg clk = 0, rst;
	reg [7:0] a_rsc_z, b_rsc_z, c_rsc_z, d_rsc_z, e_rsc_z;
	wire [63:0] CA1_return_rsc_z;
	always #15
		clk = ~clk;

	CA1 ca(a_rsc_z, b_rsc_z, c_rsc_z, d_rsc_z, e_rsc_z, CA1_return_rsc_z, clk, rst);
	initial begin
		rst = 0;
		#30;
		rst = 1;
		#30;
		rst = 0;
		#30;
		a_rsc_z = 8'd1;
		b_rsc_z = 8'd2;
		c_rsc_z = 8'd2;
		d_rsc_z = 8'd2;
		e_rsc_z = 8'd1;
		#10000;
		rst = 0;
		#30;
		rst = 1;
		#30;
		rst = 0;
		#30;
		a_rsc_z = 8'd2;
		b_rsc_z = 8'd2;
		c_rsc_z = 8'd3;
		d_rsc_z = 8'd4;
		e_rsc_z = 8'd2;
		#10000;
		$stop;
	end
endmodule
