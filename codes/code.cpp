#include <ac_int.h>

typedef ac_int<8, false> int_8;
typedef ac_int<64, false> int_64;

int_64 CA1(int_8 a, int_8 b, int_8 c, int_8 d, int_8 e){
	int_64 out = 0;
	OUTER:for(int i=0; i<5; i++){
		INNER:for(int j=0; j<10; j++){
			out += (2*a + b + c + d) / 4;
		}
		out *= e * e;
	}
	return out;
}
