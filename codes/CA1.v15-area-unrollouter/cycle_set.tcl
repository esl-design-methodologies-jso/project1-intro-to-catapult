
# Loop constraints
directive set /CA1/core/main CSTEPS_FROM {{. == 13}}
directive set /CA1/core/main/OUTER-1:INNER CSTEPS_FROM {{. == 2} {.. == 1}}
directive set /CA1/core/main/OUTER-2:INNER CSTEPS_FROM {{. == 2} {.. == 4}}
directive set /CA1/core/main/OUTER-3:INNER CSTEPS_FROM {{. == 2} {.. == 6}}
directive set /CA1/core/main/OUTER-4:INNER CSTEPS_FROM {{. == 2} {.. == 8}}
directive set /CA1/core/main/OUTER-5:INNER CSTEPS_FROM {{. == 2} {.. == 10}}

# IO operation constraints
directive set /CA1/core/main/a:io_read(a:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/b:io_read(b:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/c:io_read(c:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/d:io_read(d:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/e:io_read(e:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER:io_write(CA1.return:rsc.d) CSTEPS_FROM {{.. == 12}}

# Real operation constraints
directive set /CA1/core/main/OUTER-1:INNER/INNER:acc#8 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-1:INNER/INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-1:INNER/OUTER-1:INNER:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-1:INNER/OUTER-1:INNER:acc#4 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER-1:INNER/OUTER-1:INNER:acc#5 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-1:INNER/OUTER-1:INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-1:mul#1 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER-1:mul CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER-2:INNER/INNER:acc#10 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-2:INNER/INNER:acc#9 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-2:INNER/OUTER-2:INNER:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-2:INNER/OUTER-2:INNER:acc#4 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER-2:INNER/OUTER-2:INNER:acc#5 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-2:INNER/OUTER-2:INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-2:mul CSTEPS_FROM {{.. == 4}}
directive set /CA1/core/main/OUTER-3:INNER/INNER:acc#12 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-3:INNER/INNER:acc#11 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-3:INNER/OUTER-3:INNER:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-3:INNER/OUTER-3:INNER:acc#4 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER-3:INNER/OUTER-3:INNER:acc#5 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-3:INNER/OUTER-3:INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-3:mul CSTEPS_FROM {{.. == 6}}
directive set /CA1/core/main/OUTER-4:INNER/INNER:acc#14 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-4:INNER/INNER:acc#13 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-4:INNER/OUTER-4:INNER:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-4:INNER/OUTER-4:INNER:acc#4 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER-4:INNER/OUTER-4:INNER:acc#5 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-4:INNER/OUTER-4:INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-4:mul CSTEPS_FROM {{.. == 8}}
directive set /CA1/core/main/OUTER-5:INNER/INNER:acc#16 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-5:INNER/INNER:acc#15 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-5:INNER/OUTER-5:INNER:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-5:INNER/OUTER-5:INNER:acc#4 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER-5:INNER/OUTER-5:INNER:acc#5 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-5:INNER/OUTER-5:INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER-5:mul CSTEPS_FROM {{.. == 10}}
