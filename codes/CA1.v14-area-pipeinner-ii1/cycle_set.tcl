
# Loop constraints
directive set /CA1/core/main CSTEPS_FROM {{. == 2}}
directive set /CA1/core/main/OUTER CSTEPS_FROM {{. == 3} {.. == 1}}
directive set /CA1/core/main/OUTER/INNER CSTEPS_FROM {{. == 2} {.. == 0}}

# IO operation constraints
directive set /CA1/core/main/a:io_read(a:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/b:io_read(b:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/c:io_read(c:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/d:io_read(d:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/e:io_read(e:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/io_write(CA1.return:rsc.d) CSTEPS_FROM {{.. == 1}}

# Real operation constraints
directive set /CA1/core/main/OUTER/INNER/INNER:acc#9 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/INNER/INNER:acc#8 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/INNER/INNER:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/INNER/INNER:acc#4 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER/INNER/INNER:acc#5 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/INNER/INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/OUTER:mul#1 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/OUTER:mul CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/OUTER:acc#1 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER/OUTER:acc CSTEPS_FROM {{.. == 1}}
