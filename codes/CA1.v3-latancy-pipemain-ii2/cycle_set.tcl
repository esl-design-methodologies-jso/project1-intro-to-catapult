
# Loop constraints
directive set /CA1/core/main CSTEPS_FROM {{. == 5}}

# IO operation constraints
directive set /CA1/core/main/a:io_read(a:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/b:io_read(b:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/c:io_read(c:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/d:io_read(d:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/e:io_read(e:rsc.d) CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/io_write(CA1.return:rsc.d) CSTEPS_FROM {{.. == 4}}

# Real operation constraints
directive set /CA1/core/main/INNER:mux CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:mux#1 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:mux#2 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:mux#3 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:mux#4 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:and#1 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:and CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/INNER:acc#9 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:acc#8 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:acc#7 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:acc#4 CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/INNER:and#2 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:acc#5 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER:mul#1 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER:mul CSTEPS_FROM {{.. == 2}}
directive set /CA1/core/main/OUTER:acc#1 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/OUTER:acc CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:and#4 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:mux#11 CSTEPS_FROM {{.. == 4}}
directive set /CA1/core/main/INNER:mux#14 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:mux#12 CSTEPS_FROM {{.. == 1}}
directive set /CA1/core/main/INNER:mux#13 CSTEPS_FROM {{.. == 1}}
